﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace AWESPromotionsProviderAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "LanguageAPI",
                routeTemplate: "api/PLATFORM/{platformID}/PROMOTION/{PromotionID}/LANGUAGE/{id}",
                defaults: new { controller = "language", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "PromotionAPI",
                routeTemplate: "api/PLATFORM/{platformID}/PROMOTION/{id}",
                defaults: new { controller = "promotion", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "CodeAPI",
                routeTemplate: "api/PLATFORM/{platformID}/PROMOTION/{PromotionID}/CODE/{id}",
                defaults: new { controller="code", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "routeAPI",
                routeTemplate: "api/PLATFORM/{platformID}/PROMOTION/{PromotionID}/ROUTE/{id}",
                defaults: new { controller = "route", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "registerAPI",
                routeTemplate: "api/PLATFORM/{platformID}/REGISTER/{id}",
                defaults: new { controller = "register", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "imageAPI",
                routeTemplate: "api/PROMOTION/{PromotionID}/IMAGE/{id}",
                defaults: new { controller = "register", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "registerDetail",
                routeTemplate: "api/PLATFORM/{platformID}/REGISTER/{registerID}/REGISTERDETAIL/{id}",
                defaults: new { controller = "registerDetail", id = RouteParameter.Optional }
            );
        }
    }
}
