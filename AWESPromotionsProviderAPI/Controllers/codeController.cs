﻿using AWESCommon.Configuration;
using AWESCommon.statsSupport;
using AWESPromotionsProviderAPIModel.Promotions.Promotion;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using System.Runtime.Remoting.Messaging;

namespace AWESPromotionsProviderAPI.Controllers
{
    public class codeController : baseController
    {
        public async Task<IHttpActionResult> Get(string platformID, int PromotionID)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);
            //api/PLATFORM/{platformID}/PROMOTION/{PromotionID}/CODE/
            try
            {
                var codes = await (from i in dataContext.codes
                             where i.promotionID.platform== platformID &
                                    i.promotionID.ID == PromotionID
                                   select new { ID = i.ID, KIUCode = i.KIUcode}
                                 ).ToListAsync();
                return Ok(codes);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Get(string platformID, int PromotionID, int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);
            string promcode = GetQueryString("code");
            try
            {
                if (PromotionID == 0) {
                    List<code> codes = await (from i in dataContext.codes
                                              where i.KIUcode == promcode
                                              select i).Where(i=>!i.used).ToListAsync();
                    if (codes.Count > 0) {
                        return Ok(codes.First());
                    }
                    return Ok();
                }
                var code = await (from i in dataContext.codes
                            where i.promotionID.platform==platformID &
                            i.promotionID.ID == PromotionID & i.ID == id
                                  select new { ID = i.ID, KIUCode = i.KIUcode }
                                ).FirstAsync();
                return Ok(code);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Post(string platformID , int PromotionID, [FromBody]code value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                promotion promotion = await (from i in dataContext.promotions
                                             where i.ID == PromotionID &
                                             i.platform == platformID
                                             select i
                                     ).FirstAsync();
                value.promotionID = promotion;
                while(string.IsNullOrEmpty(value.KIUcode))
                {
                    //GENERATE CODE
                    value.KIUcode = Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
                    try
                    {
                        dataContext.codes.Add(value);
                        await dataContext.SaveChangesAsync();
                    }
                    catch (DbUpdateException)
                    {
                        value.KIUcode = "";
                    }
                }

                return Ok(value.KIUcode);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }

        }
        public async Task<IHttpActionResult> Put(string platformID, int PromotionID, int id, [FromBody]code value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null | value.ID!=id)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                promotion promotion = await (from i in dataContext.promotions
                                       where i.platform == platformID &
                                       i.ID == PromotionID
                                       select i
                                          ).FirstAsync();
                value.promotionID = promotion;
                dataContext.Entry(value).State = System.Data.Entity.EntityState.Modified;
                await dataContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Delete(string platformID, int PromotionID, int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                List<code> codes = await (from i in dataContext.codes
                                          where i.promotionID.platform == platformID &&
                                          i.promotionID.ID == PromotionID
                                          select i
                                             ).ToListAsync();
                foreach (code code in codes)
                {
                    dataContext.codes.Remove(code);
                }
                
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
    }
}
