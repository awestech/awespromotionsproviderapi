﻿using AWESCommon.Configuration;
using AWESCommon.statsSupport;
using AWESPromotionsProviderAPIModel.Promotions.Promotion;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AWESPromotionsProviderAPI.Controllers
{
    public class languageController : baseController
    {
        //api/PLATFORM/{platformID}/PROMOTION/{PromotionID}/LANGUAGE/{id}
        public async Task<IHttpActionResult> Get(string platformID, int PromotionID)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                string filter = GetQueryString("filter");
                if (string.IsNullOrEmpty(filter))
                {
                    var languages = await (from i in dataContext.languages
                                           //where i.PromotionID.platform==platformID && i.PromotionID.ID==PromotionID
                                           select new { ID = i.ID, lang = i.lang + (i.locale == "" ? "" : "-" + i.locale), order = i.order, html = i.html })
                                           .GroupBy(m => m.lang)
                                           .Select(m => m.FirstOrDefault())
                                           .OrderBy(i => i.order)
                                           .ToListAsync();
                    return Ok(languages);
                }
                else {
                    var languages = await (from i in dataContext.languages
                                           where i.PromotionID.platform==platformID && i.PromotionID.ID==PromotionID
                                           select new { ID = i.ID, lang = i.lang + (i.locale == "" ? "" : "-" + i.locale), order = i.order, html = i.html, promoID=i.PromotionID })
                                               .GroupBy(m => m.lang)
                                               .Select(m => m.FirstOrDefault())
                                               .OrderBy(i => i.order)
                                               .ToListAsync();
                    return Ok(languages);
                }
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Get(string platformID, int PromotionID, int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                var languages = await (from i in dataContext.languages
                                       where i.PromotionID.platform == platformID
                                       select new { ID = i.ID, lang = i.lang + (i.locale == "" ? "" : "-" + i.locale), order = i.order, html = i.html }).Distinct().OrderBy(i => i.order).ToListAsync();
                return Ok(languages);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Post(string platformID, int PromotionID, [FromBody]language value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                promotion promotion = await (from i in dataContext.promotions
                                             where i.platform == platformID &
                                             i.ID == PromotionID
                                             select i
                                          ).FirstAsync();
                value.lang = value.lang.ToLower();
                value.locale = value.locale.ToUpper();
                value.PromotionID = promotion;
                dataContext.languages.Add(value);
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }

        }
        public async Task<IHttpActionResult> Put(string platformID, int PromotionID, int id, [FromBody]language value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                if (value.lang.Length > 2 || value.locale.Length > 2)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                promotion promotion = await (from i in dataContext.promotions
                                             where i.platform == platformID &&
                                             i.ID == PromotionID
                                             select i
                                              ).FirstAsync();
                value.PromotionID = promotion;
                if (id != -1)
                {
                    value.lang = value.lang.ToLower();
                    value.locale = value.locale.ToUpper();
                    dataContext.Entry(value).State = EntityState.Modified;
                }
                else {
                    List<language> languages = await (from i in dataContext.languages
                                                      where i.PromotionID.ID == PromotionID
                                                      select i
                                                          ).ToListAsync();
                    value.order = languages.Count + 1;
                    value.lang = value.lang.ToLower();
                    value.locale = value.locale.ToUpper();
                    dataContext.languages.Add(value);
                }
                await dataContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Delete(int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                dataContext.languages.Remove(dataContext.languages.Find(id));
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
    }
}
