﻿using AWESCommon.Configuration;
using AWESCommon.statsSupport;
using AWESPromotionsProviderAPIModel.Promotions.Promotion;
using AWESPromotionsProviderAPIModel.Registers.Register;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AWESPromotionsProviderAPI.Controllers
{
    public class promotionController : baseController
    {
        public async Task<IHttpActionResult> Get(string platformID)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            //api/PLATFORM/{platformID}/REGISTER/{id}
            try
            {
                string code= GetQueryString("code");
                if (!string.IsNullOrEmpty(code)) {
                    if (code.Length > 2) {
                        code = code.Substring(0,2);
                    }
                    var promotionCode = await (from i in dataContext.promotions
                                           where i.platform == platformID
                                           && i.KIUCode.ToString().ToLower()==code.ToLower()
                                           select i
                                  ).ToListAsync();
                    promotionCode = promotionCode.Select(i => i).Reverse().ToList();
                    return Ok(promotionCode[0]);
                }
                var promotion = await (from i in dataContext.promotions
                                       where i.platform == platformID
                                       select i
                                  ).ToListAsync();
                return Ok(promotion);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Get(string platformID, int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                var promotion = await (from i in dataContext.promotions
                                       where i.platform == platformID &
                                       i.ID == id
                                       select i
                                ).FirstAsync();
                return Ok(promotion);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Post(string platformID, [FromBody]promotion value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                int quantity = 0;
                int.TryParse(GetQueryString("quantity"), out quantity);
                string keyWord = GetQueryString("keyWord");
                if (value.codeType == "Unique")
                {
                    code code = new code();
                    code.ID = 0;
                    code.KIUcode = value.KIUCode + keyWord;
                    code.promotionID = value;
                    dataContext.codes.Add(code);
                    dataContext.promotions.Add(value);
                    await dataContext.SaveChangesAsync();
                }
                else
                {
                    //GENERATE AUTOCODES
                    List<string> codes = await this.codeGenarate(value.KIUCode, quantity, platformID);
                    foreach (var codeGenerated in codes)
                    {
                        code code = new code();
                        code.ID = 0;
                        code.KIUcode = codeGenerated;
                        code.promotionID = value;
                        dataContext.codes.Add(code);
                    }
                    value.KIUCode = value.KIUCode.Trim();
                    dataContext.promotions.Add(value);
                    await dataContext.SaveChangesAsync();
                }
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }

        }
        public async Task<IHttpActionResult> Put(string platformID, int id, [FromBody]promotion value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null | id != value.ID)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                int quantity = 0;
                int.TryParse(GetQueryString("quantity"), out quantity);
                string keyWord = GetQueryString("keyWord");
                if (value.codeType != "Unique")
                {
                    //GENERATE AUTOCODES
                    List<string> codes = await this.codeGenarate(value.KIUCode, quantity, platformID);
                    foreach (var codeGenerated in codes)
                    {
                        code code = new code();
                        code.ID = 0;
                        code.KIUcode = codeGenerated;
                        code.promotionID = value;
                        dataContext.codes.Add(code);
                    }
                    dataContext.promotions.Add(value);
                }
                else
                {
                    code code = new code();
                    try
                    {
                        code = await (from i in dataContext.codes
                                      where i.promotionID.ID == id
                                      select i
                                                          ).FirstAsync();
                        code.KIUcode = code.KIUcode.Substring(0, 2) + keyWord;
                        dataContext.Entry(code).State = EntityState.Modified;
                    }
                    catch (Exception)
                    {
                        code.ID = 0;
                        code.KIUcode = value.KIUCode + keyWord;
                        code.promotionID = value;
                        dataContext.codes.Add(code);
                    }
                }
                value.KIUCode = value.KIUCode.Trim();
                dataContext.Entry(value).State = EntityState.Modified;
                await dataContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Delete(int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                var codes = await (from i in dataContext.codes
                                   where i.promotionID.ID == id
                                   select i
                                     ).ToListAsync();
                var routes = await (from i in dataContext.routes
                                    where i.promotionID.ID == id
                                    select i
                                       ).ToListAsync();
                var languages = await (from i in dataContext.languages
                                       where i.PromotionID.ID == id
                                       select i
                                           ).ToListAsync();
                foreach (var code in codes)
                {
                    dataContext.codes.Remove(code);
                }
                foreach (var route in routes)
                {
                    dataContext.routes.Remove(route);
                }
                foreach (var language in languages)
                {
                    dataContext.languages.Remove(language);
                }
                dataContext.promotions.Remove(dataContext.promotions.Find(id));
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        private async Task<List<string>> codeGenarate(string KIUCode, int quantity, string platformID)
        {
            List<string> codes = new List<string>();
            DateTime codePattern = DateTime.Now;
            List<code> stack = await (from i in dataContext.codes
                                      where i.promotionID.platform == platformID &&
                                      i.KIUcode.Trim().Substring(0, 2) == KIUCode
                                      select i
                           ).ToListAsync();
            for (int i = 1; i <= quantity; i++)
            {
                string code = KIUCode + codePattern.Year + codePattern.Day + codePattern.Month;
                codes.Add(code);
                codePattern = codePattern.AddDays(1);
            }
            if (quantity == stack.Count)
            {
                return new List<string>();
            }
            // ADDING MORE CODES TO THE PROMOTION
            if (quantity > stack.Count)
            {
                List<string> codesLeft = new List<string>();
                int difference = 0;
                for (int i = 1; i <= quantity + stack.Count; i++)
                {
                    string code = KIUCode + codePattern.Year + codePattern.Day + codePattern.Month;
                    if (!codes.Contains(code))
                    {
                        //IF THE difference IS GREATER THAN quantity - stack.Count, ALL THE CODES HAVE BEEN CREATED
                        if (difference <= quantity - stack.Count)
                        {
                            codesLeft.Add(code);
                            difference++;
                        }
                    }
                    codePattern = codePattern.AddDays(1);
                    if (difference >= quantity - stack.Count)
                    {
                        break;
                    }
                }
                return codesLeft;
            }
            return codes;


        }
    }
}