﻿using AWESCommon.Configuration;
using AWESCommon.statsSupport;
using AWESPromotionsProviderAPIModel.Registers.Register;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AWESPromotionsProviderAPI.Controllers
{
    public class registerController : baseController
    {
        public async Task<IHttpActionResult> Get(string platformID)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            //api/PLATFORM/{platformID}/REGISTER/{id}
            try
            {
                var registers = await (from i in dataContext.registers
                                       where i.platform == platformID
                                select i
                                  ).ToListAsync();
                return Ok(registers);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Get(string platformID, int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                var register = await (from i in dataContext.registers
                                    where i.platform == platformID &
                                    i.ID == id
                                    select i
                                ).FirstAsync();
                return Ok(register);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Post(int platformID, [FromBody]register value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }

                dataContext.registers.Add(value);
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }

        }
        public async Task<IHttpActionResult> Put(int platformID, int id, [FromBody]register value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null | id!=value.ID)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                dataContext.Entry(value).State = EntityState.Modified;
                await dataContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Delete(int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                dataContext.registers.Remove(dataContext.registers.Find(id));
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
    }
}
