﻿using AWESCommon.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Data;
using AWESCommon.statsSupport;
using AWESPromotionsProviderAPIModel.Registers.Register;

namespace AWESPromotionsProviderAPI.Controllers
{
    public class registerDetailController : baseController
    {
        public async Task<IHttpActionResult> Get(string platformID, int registerID)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);
            //api/PLATFORM/{platformID}/PROMOTION/{PromotionID}/CODE/
            try
            {
                List<registerDetail> registerDetail = new List<registerDetail>();

                string email = GetQueryString("email");

                if (!string.IsNullOrWhiteSpace(email)) {

                    registerDetail = await (from i in dataContext.registerDetails
                                            where i.platform == platformID &&
                                                 i.registerID == registerID &&
                                                 i.email == email
                                            select i
                                 ).ToListAsync();
                    return Ok(registerDetail);
                }

                registerDetail = await (from i in dataContext.registerDetails
                                   where i.platform == platformID &&
                                        i.registerID == registerID
                                   select i
                                 ).ToListAsync();
                return Ok(registerDetail);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Get(string platformID, int registerID, int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                registerDetail registerDetail = await (from i in dataContext.registerDetails
                                  where i.ID== id
                                  select i
                                ).FirstOrDefaultAsync();
                return Ok(registerDetail);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Post(string platformID, int registerID, [FromBody]registerDetail value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                string list = GetQueryString("list");
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                dataContext.registerDetails.Add(value);
                await dataContext.SaveChangesAsync();

                if (!string.IsNullOrEmpty(list)) {
                    List<registerDetail> registersDetails = await (from i in dataContext.registerDetails
                                                             where i.platform == platformID &&
                                                                  i.registerID == registerID
                                                             select i
                                                               ).ToListAsync();
                    return Ok(registersDetails);
                }

                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }

        }
        public async Task<IHttpActionResult> Put(string platformID, int registerID, int id, [FromBody]registerDetail value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                string list = GetQueryString("list");
                if (value == null | value.ID != id)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                registerDetail registerDetail = await (from i in dataContext.registerDetails
                                             where i.ID==id 
                                             select i
                                          ).FirstOrDefaultAsync();
                registerDetail.registerID = value.registerID;
                registerDetail.platform = value.platform;
                dataContext.Entry(value).State = System.Data.Entity.EntityState.Modified;
                await dataContext.SaveChangesAsync();
                if (!string.IsNullOrEmpty(list)) {
                    List<registerDetail> registersDetails = await (from i in dataContext.registerDetails
                                                                   where i.platform == platformID &&
                                                                        i.registerID == registerID
                                                                   select i
                                                                   ).ToListAsync();
                    return Ok(registersDetails);
                }
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Delete(string platformID, int registerID, int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                string list = GetQueryString("list");
                registerDetail registerDetail= await (from i in dataContext.registerDetails
                                  where i.ID == id
                                  select i
                                    ).FirstOrDefaultAsync();

                dataContext.registerDetails.Remove(registerDetail);
                await dataContext.SaveChangesAsync();
                if (!string.IsNullOrEmpty(list)) {
                    List<registerDetail> registersDetails = await (from i in dataContext.registerDetails
                                                                   where i.platform == platformID &&
                                                                        i.registerID == registerID
                                                                   select i
                                                                   ).ToListAsync();
                    return Ok(registersDetails);
                }
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
    }
}
