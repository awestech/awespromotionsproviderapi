﻿using AWESCommon.Configuration;
using AWESCommon.statsSupport;
using AWESPromotionsProviderAPIModel.Promotions.Promotion;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AWESPromotionsProviderAPI.Controllers
{
    public class routeController : baseController
    {
        public async Task<IHttpActionResult> Get(string platformID,int PromotionID)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);
            //api/PLATFORM/{platform}/PROMOTION/{PromotionID}/ROUTE/{id}
            try
            {
                var routes = await (from i in dataContext.routes
                                   where i.promotionID.platform==platformID &
                                   i.promotionID.ID == PromotionID
                                   select new {ID=i.ID, origin=i.origin, destination=i.destination }
                                 ).ToListAsync();
                return Ok(routes);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Get(string platformID, int PromotionID, int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                var routes = await (from i in dataContext.routes
                                  where i.promotionID.platform==platformID &
                                  i.promotionID.ID == PromotionID &
                                  i.ID == id
                                    select new { ID = i.ID, origin = i.origin, destination = i.destination }
                                ).FirstAsync();
                return Ok(routes);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Post(string platformID, int PromotionID, [FromBody]route value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                promotion promotion = await (from i in dataContext.promotions
                                             where i.platform==platformID &
                                             i.ID == PromotionID
                                             select i
                                     ).FirstAsync();
                value.promotionID = promotion;
                route route = null;
                try
                {
                    route = await (from i in dataContext.routes
                                   where i.promotionID.ID == value.promotionID.ID &
                                   (i.origin == value.origin && i.destination == value.destination) 
                                   select i
                                            ).FirstAsync();
                }
                catch (InvalidOperationException)
                {
                    ;
                } 
                if (route == null && value.origin != value.destination)
                {
                    dataContext.routes.Add(value);
                    await dataContext.SaveChangesAsync();
                }
                else {
                    return Ok(HttpStatusCode.BadRequest);
                }
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }

        }
        public async Task<IHttpActionResult> Put(string platformID, int PromotionID, int id, [FromBody]route value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                promotion promotion = await (from i in dataContext.promotions
                                             where i.platform == platformID &
                                             i.ID == PromotionID
                                             select i
                                          ).FirstAsync();
                value.promotionID = promotion;
                dataContext.Entry(value).State = EntityState.Modified;
                await dataContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Delete(string platformID, int PromotionID, int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                dataContext.routes.Remove(dataContext.routes.Find(id));
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
    }
}
