﻿using AWESCommon.Configuration;
using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AWESCommon.statsSupport;
using AWESPromotionsProviderAPIModel.Promotions.Promotion;

namespace AWESPromotionsProviderAPI.Controllers
{
    public class stateController : baseController
    {
        public async Task<IHttpActionResult> Get()
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            //api/PLATFORM/{platformID}/REGISTER/{id}
            try
            {
                var states = await (from i in dataContext.states
                                       select i
                                  ).ToListAsync();
                return Ok(states);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Get(int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                var states = await (from i in dataContext.states
                                      where i.ID == id
                                      select i
                                ).FirstAsync();
                return Ok(states);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Post([FromBody]state value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }

                dataContext.states.Add(value);
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }

        }
        public async Task<IHttpActionResult> Put(int id, [FromBody]state value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null | id != value.ID)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                dataContext.Entry(value).State = System.Data.Entity.EntityState.Modified;
                await dataContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        public async Task<IHttpActionResult> Delete(int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                dataContext.states.Remove(dataContext.states.Find(id));
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
    }
}
