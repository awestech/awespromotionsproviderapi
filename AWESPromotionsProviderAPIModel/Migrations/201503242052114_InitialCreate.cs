namespace AWESPromotionsProviderAPIModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.codes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        KIUcode = c.String(maxLength: 50),
                        promotionID_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.promotions", t => t.promotionID_ID)
                .Index(t => t.promotionID_ID);
            
            CreateTable(
                "dbo.promotions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        platform = c.String(maxLength: 50),
                        name = c.String(maxLength: 50),
                        KIUCode = c.String(maxLength: 50),
                        codeType = c.String(maxLength: 50),
                        initDateSale = c.DateTime(nullable: false),
                        endDateSale = c.DateTime(nullable: false),
                        initDateFly = c.DateTime(nullable: false),
                        endDateFly = c.DateTime(nullable: false),
                        state = c.String(maxLength: 10),
                        discountPercentage = c.Double(nullable: false),
                        discountValue = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.languages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        lang = c.String(),
                        locale = c.String(),
                        order = c.Int(nullable: false),
                        html = c.String(),
                        PromotionID_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.promotions", t => t.PromotionID_ID)
                .Index(t => t.PromotionID_ID);
            
            CreateTable(
                "dbo.registers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        platform = c.String(maxLength: 50),
                        name = c.String(maxLength: 50),
                        initDateRegister = c.DateTime(nullable: false),
                        endDateRegister = c.DateTime(nullable: false),
                        state = c.String(maxLength: 10),
                        discountPercentage = c.Int(nullable: false),
                        discountValue = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.routes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        origin = c.String(maxLength: 50),
                        destination = c.String(maxLength: 3),
                        promotionID_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.promotions", t => t.promotionID_ID)
                .Index(t => t.promotionID_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.routes", "promotionID_ID", "dbo.promotions");
            DropForeignKey("dbo.languages", "PromotionID_ID", "dbo.promotions");
            DropForeignKey("dbo.codes", "promotionID_ID", "dbo.promotions");
            DropIndex("dbo.routes", new[] { "promotionID_ID" });
            DropIndex("dbo.languages", new[] { "PromotionID_ID" });
            DropIndex("dbo.codes", new[] { "promotionID_ID" });
            DropTable("dbo.routes");
            DropTable("dbo.registers");
            DropTable("dbo.languages");
            DropTable("dbo.promotions");
            DropTable("dbo.codes");
        }
    }
}
