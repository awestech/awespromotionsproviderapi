namespace AWESPromotionsProviderAPIModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MaxLengthRoute : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.routes", "origin", c => c.String(maxLength: 3));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.routes", "origin", c => c.String(maxLength: 50));
        }
    }
}
