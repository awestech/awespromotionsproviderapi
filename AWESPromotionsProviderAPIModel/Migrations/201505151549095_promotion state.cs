namespace AWESPromotionsProviderAPIModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class promotionstate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.states",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(maxLength: 50),
                        description = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.states");
        }
    }
}
