namespace AWESPromotionsProviderAPIModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class registerDetail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.registerDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        registerID = c.Int(nullable: false),
                        platform = c.String(),
                        name = c.String(),
                        email = c.String(),
                        telephone = c.String(),
                        documentNumber = c.String(),
                        date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.registerDetails");
        }
    }
}
