namespace AWESPromotionsProviderAPIModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class KIUCodeMax : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.promotions", "KIUCode", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.promotions", "KIUCode", c => c.String(maxLength: 50));
        }
    }
}
