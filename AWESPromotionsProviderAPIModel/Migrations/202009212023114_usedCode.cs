namespace AWESPromotionsProviderAPIModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usedCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.codes", "used", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.codes", "used");
        }
    }
}
