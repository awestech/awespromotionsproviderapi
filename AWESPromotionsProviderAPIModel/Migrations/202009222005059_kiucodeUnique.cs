namespace AWESPromotionsProviderAPIModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kiucodeUnique : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.codes", "KIUcode", unique: true, name: "KIUCodeIndex");
        }
        
        public override void Down()
        {
            DropIndex("dbo.codes", "KIUCodeIndex");
        }
    }
}
