﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESPromotionsProviderAPIModel.Promotions.Promotion
{
    public class code
    {
        public int ID { get; set; }

        [Display(Name = "Promotion ID")]
        public virtual promotion promotionID { get; set; }
        [Display(Name = "code")]
        [MaxLength(50, ErrorMessage = "Max length is 50")]
        [Index("KIUCodeIndex",IsUnique = true)]
        public string KIUcode { get; set; }
        [Display(Name = "Used")]
        public bool used { get; set; }
    }
}
