﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESPromotionsProviderAPIModel.Promotions.Promotion
{
    public class promotion
    {
        public int ID { get; set; }

        [Display(Name = "Platform")]
        [MaxLength(50, ErrorMessage = "Max length is 50")]
        public string platform { get; set; }
        [Display(Name = "Promotion name")]
        [MaxLength(50, ErrorMessage = "Max length is 50")]
        public string name { get; set; }
        public string KIUCode { get; set; }
        [Display(Name = "Code Type")]
        [MaxLength(50, ErrorMessage = "Max length is 10")]
        public string codeType { get; set; }
        [Display(Name = "Init date sale")]
        public DateTime initDateSale { get; set; }
        [Display(Name = "End date sale")]
        public DateTime endDateSale { get; set; }
        [Display(Name = "Init date fly")]
        public DateTime initDateFly { get; set; }
        [Display(Name = "End date fly")]
        public DateTime endDateFly { get; set; }
        [Display(Name = "state")]
        [MaxLength(10, ErrorMessage = "Max length is 10")]
        public string state { get; set; }
        [Display(Name = "Discount Percentage")]
        public double discountPercentage { get; set; }
        [Display(Name = "Discount Value")]
        public double discountValue { get; set; }
    }
}
