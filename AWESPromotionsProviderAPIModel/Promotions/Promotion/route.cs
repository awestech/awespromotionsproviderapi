﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESPromotionsProviderAPIModel.Promotions.Promotion
{
    public class route
    {
        public int ID { get; set; }

        [Display(Name = "Promotion ID")]
        public virtual promotion promotionID { get; set; }
        [Display(Name = "Origin")]
        [MaxLength(3, ErrorMessage = "Max length is 3")]
        public string origin { get; set; }
        [Display(Name = "Destination")]
        [MaxLength(3, ErrorMessage = "Max length is 3")]
        public string destination { get; set; }
    }
}
