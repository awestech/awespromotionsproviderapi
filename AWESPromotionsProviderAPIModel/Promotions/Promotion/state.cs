﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESPromotionsProviderAPIModel.Promotions.Promotion
{
    public class state
    {
        public int ID { get; set; }

        [Display(Name = "state name")]
        [MaxLength(50, ErrorMessage = "Max length is 50")]
        public string name { get; set; }
        [Display(Name = "state description")]
        [MaxLength(512, ErrorMessage = "Max length is 512")]
        public string description { get; set; }
    }
}
