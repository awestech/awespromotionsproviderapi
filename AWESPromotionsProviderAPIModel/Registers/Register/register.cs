﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESPromotionsProviderAPIModel.Registers.Register
{
    public class register
    {
        public int ID { get; set; }

        [Display(Name = "Platform")]
        [MaxLength(50, ErrorMessage = "Max length is 50")]
        public string platform { get; set; }
        [Display(Name = "Register name")]
        [MaxLength(50, ErrorMessage = "Max length is 50")]
        public string name { get; set; }
        [Display(Name = "Init date register")]
        public DateTime initDateRegister { get; set; }
        [Display(Name = "End date register")]
        public DateTime endDateRegister { get; set; }
        [Display(Name = "state")]
        [MaxLength(10, ErrorMessage = "Max length is 10")]
        public string state { get; set; }
        [Display(Name = "Discount Percentage")]
        public int discountPercentage { get; set; }
        [Display(Name = "Discount Value")]
        public double discountValue { get; set; }
    }
}
