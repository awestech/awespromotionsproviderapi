﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESPromotionsProviderAPIModel.Registers.Register
{
    public class registerDetail
    {
        public int ID { get; set; }
        public int registerID { get; set; }
        public string platform { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string telephone { get; set; }
        public string documentNumber { get; set; }
        public DateTime date { get; set; }
    }
}
