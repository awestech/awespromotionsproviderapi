﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESPromotionsProviderAPIModel.Registers.Register
{
    class registerLanguage
    {
        public int ID { get; set; }
        public string lang { get; set; }
        public string locale { get; set; }
        public int order { get; set; }
        public string html { get; set; }
        public virtual register PromotionID { get; set; }
    }
}
