﻿using AWESPromotionsProviderAPIModel.Promotions.Promotion;
using AWESPromotionsProviderAPIModel.Registers.Register;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AWESPromotionsProviderAPIModel
{
    public class dataContext : DbContext
    {
        public dataContext() : base("defaultConnectionString") { }
        public dataContext(string connString) : base(connString) { }
        public DbSet<code> codes { get; set; }
        public DbSet<route> routes { get; set; }
        public DbSet<promotion> promotions { get; set; }
        public DbSet<register> registers { get; set; }
        public DbSet<registerDetail> registerDetails { get; set; }
        public DbSet<language> languages { get; set; }
        public DbSet<state> states { get; set; }
    }
}
